package com.jt.common.pojo;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain=true)
public class ItemCat extends BasePojo{
	/**
	 * 
	 */
	private static final long serialVersionUID = -630200513898459315L;
	private Long id;
	private Long parentId;
	private String name;
	private Integer status;
	private Integer sortOrder;
	private Boolean isParent;

}
