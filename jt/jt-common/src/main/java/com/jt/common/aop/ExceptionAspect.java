package com.jt.common.aop;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.jt.common.vo.SysResult;

import lombok.extern.slf4j.Slf4j;


//@ControllerAdvice		//定义controller切面
@RestControllerAdvice
@Slf4j
public class ExceptionAspect {

	@ExceptionHandler(RuntimeException.class)
	public SysResult result(Exception exception) {
		exception.printStackTrace();
		log.error("错误信息：",exception);
		return SysResult.fail();
		
	}
}
