package com.jt.vo;

import java.util.List;

import com.jt.common.pojo.Item;

import lombok.Data;

@Data
public class EasyUITable {

	private Integer total;
	private List<Item> rows;
	public EasyUITable(int total, List<Item> rows) {
		super();
		this.total = total;
		this.rows = rows;
	}
}
