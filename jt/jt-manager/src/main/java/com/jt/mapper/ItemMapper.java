package com.jt.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.jt.common.pojo.Item;

@Mapper
public interface ItemMapper {

	@Select("select count(*) from tb_item")
	int getRowCount();

	List<Item> findPageObjects(Integer start, Integer rows);

	void saveItem(Item item);

	void deleteItems(Long[] ids);

}
