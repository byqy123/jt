package com.jt.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.jt.common.pojo.ItemCat;

@Mapper
public interface ItemCatMapper {

	@Select("select name from tb_item_cat where id=#{id}")
	String findCatNameById(Long id);

	List<ItemCat> findItemCatByParentId(Long parentId);

	
}
