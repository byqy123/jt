package com.jt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jt.common.pojo.Item;
import com.jt.common.vo.SysResult;
import com.jt.service.ItemService;
import com.jt.vo.EasyUITable;

@RestController
@RequestMapping("/item")
public class ItemController {

	@Autowired
	private ItemService itemService;

	@RequestMapping("/query")
	public EasyUITable findItemPages(Integer page, Integer rows) {

		return itemService.findItemPages(page, rows);

	}
	
	@RequestMapping("/save")
	public SysResult saveItem(Item item) {
		itemService.saveItem(item);		
		return SysResult.success();		
	}
	
	@RequestMapping("/delete")
	public SysResult deleteItems(Long[] ids) {
		itemService.deleteItem(ids);
		return SysResult.success();
		
	}


}
