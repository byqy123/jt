package com.jt.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.jt.service.ItemCatService;
import com.jt.vo.EasyUITree;

@RestController
@RequestMapping("/itemCat")
public class ItemCatController {
	
	@Autowired
	private ItemCatService itemCatService;
	
	@RequestMapping("/findCatNameById")
	public String findCatNameById(Long id) {
		
		return itemCatService.findCatNameById(id);		
	}
	
	@RequestMapping("/list")
	public List<EasyUITree> findItemCatObjects(
							@RequestParam(name="id",defaultValue = "0") Long parentId){
		
		return itemCatService.findItemCatObjects(parentId);
	}
	
	

}
