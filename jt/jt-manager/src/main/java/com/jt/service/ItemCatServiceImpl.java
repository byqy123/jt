package com.jt.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jt.common.pojo.ItemCat;
import com.jt.mapper.ItemCatMapper;
import com.jt.vo.EasyUITree;

@Service
public class ItemCatServiceImpl implements ItemCatService{

	@Autowired
	private ItemCatMapper itemCatMapper;

	@Override
	public String findCatNameById(Long id) {

		return itemCatMapper.findCatNameById(id);
	}

	@Override
	public List<EasyUITree> findItemCatObjects(Long parentId) {
		
		List<EasyUITree> treeList = new ArrayList<EasyUITree>();
		List<ItemCat> catList = findItemCatList(parentId);
		
		for(ItemCat itemCat:catList) {
			
			EasyUITree easyUITree = new EasyUITree();
			String state = itemCat.getIsParent()?"closed":"open";
			easyUITree.setId(itemCat.getId());
			easyUITree.setText(itemCat.getName());
			easyUITree.setState(state);
			treeList.add(easyUITree);
			
		}
		return treeList;
	}

	private List<ItemCat> findItemCatList(Long parentId) {

		return itemCatMapper.findItemCatByParentId(parentId);
	}
}
