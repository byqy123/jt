package com.jt.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jt.common.pojo.Item;
import com.jt.mapper.ItemMapper;
import com.jt.vo.EasyUITable;

@Service
public class ItemServiceImpl implements ItemService {
	
	@Autowired
	private ItemMapper itemMapper;

	@Override
	public EasyUITable findItemPages(Integer page, Integer rows) {
		
		Integer total = itemMapper.getRowCount();
		//分页查询
		Integer start=(page-1)*rows;
		List<Item> list=itemMapper.findPageObjects(start, rows);
		
		return new EasyUITable(total,list);
	}

	@Override
	public void saveItem(Item item) {
		
		item.setStatus(1);
		item.setCreated(new Date());
		item.setUpdated(item.getCreated());
		itemMapper.saveItem(item);		
	}

	@Override
	public void deleteItem(Long[] ids) {
		
		itemMapper.deleteItems(ids);		
	}

}
