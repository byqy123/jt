package com.jt.service;

import com.jt.common.pojo.Item;
import com.jt.vo.EasyUITable;

public interface ItemService {

	EasyUITable findItemPages(Integer page, Integer rows);

	void saveItem(Item item);

	void deleteItem(Long[] ids);

}
