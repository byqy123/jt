package com.jt.service;

import java.util.List;

import com.jt.vo.EasyUITree;

public interface ItemCatService {

	String findCatNameById(Long id);

	List<EasyUITree> findItemCatObjects(Long parentId);

}
