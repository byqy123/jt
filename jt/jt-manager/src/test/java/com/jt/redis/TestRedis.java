package com.jt.redis;

import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Transaction;

public class TestRedis {
	
	private Jedis jedis;
	
	public void init() {
		String host = "192.168.38.128";
		int port = 6379;
		jedis = new Jedis(host, port);		
	}

	@Test
	public void testString() {
		String host = "192.168.38.128";
		int port = 6379;
		Jedis jedis = new Jedis(host, port);
		//jedis.setex("2021",100,"redis11111111");
		//jedis.set("2021", "kshdfgioshgsdg", "NX","EX",30*60);
		jedis.set(host, host, null);
		System.out.println("获取数据:"+jedis.get("2021"));
	}
	
	@Test
	public void testTx() {
		Transaction transaction = jedis.multi();
		try {
			//redis事务 
			transaction.set("aa", "aa");
			transaction.set("bb", "bb");
			transaction.set("vv", "vv");
			//事务提交
			transaction.exec();
		} catch (Exception e) {
			//事务回滚
			transaction.discard();
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	

}
